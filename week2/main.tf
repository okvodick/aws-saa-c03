terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    pgp = {
      source = "ekristen/pgp"
    }
  }
}

provider "aws" {
  region = "ap-southeast-1"  # Set your desired AWS region
  profile = "iamadmin-general"
}

# Create S3 bucket
resource "aws_s3_bucket" "catpics" {
  bucket = "phunn4-catpics-bucket"
}

resource "aws_s3_bucket" "animalpics" {
  bucket = "phunn4-animalpics-bucket"
}

# Create IAM User
resource "aws_iam_user" "sally" {
  name = "sally"
}

resource "aws_iam_access_key" "user_access_key" {
  user = aws_iam_user.sally.name
}

resource "pgp_key" "user_login_key" {
  name = "sally"
  email = "sally@abc.com"
  comment = "PGP Key for sally"
}

resource "aws_iam_user_login_profile" "user_login_profile" {
  user = aws_iam_user.sally.name
  pgp_key = pgp_key.user_login_key.public_key_base64
  password_reset_required = true
}

data "pgp_decrypt" "user_password_decrypt" {
  ciphertext = aws_iam_user_login_profile.user_login_profile.encrypted_password
  ciphertext_encoding = "base64"
  private_key = pgp_key.user_login_key.private_key
}

# Create IAM Group
resource "aws_iam_group" "developer-group" {
  name = "Developers"
}

resource "aws_iam_group_membership" "group_membership" {
  name = "test-group-membership"
  users = [ aws_iam_user.sally.name ]
  group = aws_iam_group.developer-group.name
}

# create IAM policy
data "aws_iam_policy_document" "policy_statement" {
  statement {
    actions = [ "s3:*" ]
    resources = [ "*" ]
    effect = "Allow"
  }
  statement {
    actions = [ "s3:*" ]
    resources = [ aws_s3_bucket.catpics.arn, "${aws_s3_bucket.catpics.arn}/*" ]
    effect = "Deny"
  }
}
resource "aws_iam_policy" "policy" {
  name = "test_policy"
  path = "/"
  description = "Policy created by phunn4 using terraform"
  policy = data.aws_iam_policy_document.policy_statement.json
}

resource "aws_iam_user_policy_attachment" "change_pwd_policy_attachment" {
  user = aws_iam_user.sally.name
  policy_arn = "arn:aws:iam::aws:policy/IAMUserChangePassword"
}

resource "aws_iam_group_policy_attachment" "developer_group_policy_attachment" {
  group = aws_iam_group.developer-group.name
  policy_arn = aws_iam_policy.policy.arn
}

output "credential" {
  value = {
    key = aws_iam_access_key.user_access_key.id
    secret = aws_iam_access_key.user_access_key.secret
    password = data.pgp_decrypt.user_password_decrypt.plaintext
  }
  sensitive = true
}